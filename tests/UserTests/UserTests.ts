import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../src/server';
import MongoUserOps from '../../src/users/userDb';

chai.use(chaiHttp);
const should = chai.should();

const userDb = new MongoUserOps();
process.env.DB = 'mongodb://localhost:27017/survey_test';
process.env.PORT = '7493';

after((done) => {
    userDb.cleanup().then(() => done());
});
before((done) => {
    userDb.cleanup().then(() => done());
});

describe('Signup Flow', () => {

    const properUser = {
        email: 'jim.hopper@hawkinsdept.com', password: 'CoffeeAndContepmlation', userName: 'jim.hopper',
    };
    const improperUserWrongEmail = {
        email: 'demogorgon@upsidedown', password: '12356789', userName: 'deomo-dog',
    };
    const improperUserShortPassword = {
        email: 'demogorgon@upsidedown.com', password: 'screech', userName: 'deomo-dog',
    };
    it('Should be signed up', () => {
        chai.request(app)
            .post('/users/signup')
            .send(properUser)
            .end((error: any, response: any) => {
                response.should.have.status(200);
                response.body.should.have.property('email');
                response.body.should.not.have.any.keys('password');
                response.body.should.have.all.keys('_id', 'email', 'token', 'userName', 'userType');
                response.body.token.should.deep.equal('');
            });
    });

    it('Should not sign up and show wrong email error', () => {
        chai.request(app)
            .post('/users/signup')
            .send(improperUserWrongEmail)
            .end((error: any, response: any) => {
                response.should.not.have.status(200);
                response.body.should.have.property('messages');
                response.body.messages.should.be.an('array');
                response.body.messages[0].should.have.string('Should be proper email');
            });
    });
});
