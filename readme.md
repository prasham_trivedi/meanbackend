## Survey Backend Code

Survey backend code with Mongo DB, Express and Typescript.

How to run it.

From command line.

- `cd` to current directory
- Enter `npm run run-all` from command line.
- Continue coding.

From Visual Studio Code

- Run task `run all`.

