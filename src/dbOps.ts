interface DbOps {
    init(): void;
    save(document: string, data: any): Promise<any>;
    get(document: string): Promise<any[]>;
    getById(document: string, id: string): Promise<any>;
    clear(document: string): Promise<void>;
}
