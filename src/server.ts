import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import questions from './questions/questions';
import surveys from './surveys/surveys';
import users from './users/users';
import { initDb } from './utils';

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use('/users', users);
app.use('/surveys', surveys);
app.use('/questions', questions);

app.get('/', (req, res) => {
    res.send('ok, Hello there');
});

app.listen(process.env.PORT || 2488, () => {
    initDb();
    console.log(`Listening on ${process.env.PORT}`);
});

export default app;
