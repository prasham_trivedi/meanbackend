export const appSecret = 'toInfinityAndBeyond';
export const DB = 'mongodb://localhost:27017/survey';
export const allowedTypes = ['text', 'radio', 'check', 'number', 'dropdown'];
