import dotenv from 'dotenv';
import { Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import { appSecret, DB } from './config';

const secret = appSecret;

export const validator = (request: Request, response: Response, next: any) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        console.log(JSON.stringify(errors.array()));
        return response.status(400).json({
            messages: errors.array().map((error: any) => {
                return `${error.param}: ${error.msg}`;
            }),
        });
    } else {
        next();
    }

};

export const tokenChecker = (request: Request, response: Response, next: any) => {
    const token = request.body.token || request.query.token || request.headers['x-access-token']
        || request.headers.authorization;
    if (token) {
        jwt.verify(token, secret, (error: any, decoded: any) => {
            if (error) {
                response.status(401).send({ message: 'You are not authorised to make this request' });
            } else {
                request.params.decoded = decoded;
                next();
            }
        });
    } else {
        return response.status(403).send({ message: 'No token provided.' });
    }

};

export function getToken(user: any) {
    return jwt.sign({
        email: user.email, id: user._id,
        isAdmin: user.userType.isAdmin, userType: user.userType.type,
    }, secret);
}

export function errorProcessor(response: Response): (reason: any) => void | PromiseLike<void> {
    return (error) => {
        console.log(error);
        if (error.code === undefined) {
            error.code = 500;
            error.message = 'Something went wrong';
        }
        response.status(error.code).send(error);
    };
}

export function initDb() {
    dotenv.config();
    console.log(process.env.DB);
    mongoose.Promise = global.Promise;
    mongoose.connect(process.env.DB, { useNewUrlParser: true }).then(() => console.log(`DB is got connected`)).
        catch((error: any) => console.log(`something went wrong ${error}`));
}
