import { ObjectID, ObjectId } from 'bson';
import mongoose from 'mongoose';
import { Question, QuestoinsSchema, Survey, SurveySchema, Vote, Votes } from '../models';

export default class MongoSurveyOps {
    private Survey = mongoose.model<Survey>('surveys', SurveySchema);
    private Questions = mongoose.model<Question>('question', QuestoinsSchema);
    private Votes = mongoose.model<Votes>('vote', Vote);
    constructor() {
        mongoose.set('debug', true);
    }

    public getById(id: string, isMini: boolean): Promise<any> {
        let query = this.Survey.findById(id);
        if (!isMini) {
            query = query.populate('createdBy', '_id email');
        }
        return query.exec().catch((error) => {
            throw { code: 500, message: `Problem Getting Survey`, technicalMessage: error.message };
        });
    }

    public getAll(isMini: boolean): Promise<any[]> {
        let query = this.Survey.find();
        if (!isMini) {
            query = query.populate('createdBy', '_id email');
        }
        return query.exec().catch((error) => {
            throw { code: 500, message: `Problem Getting Surveys`, technicalMessage: error.message };
        });
    }

    public createSurvey(data: any): Promise<any> {
        const survey = new this.Survey({
            createdBy: new ObjectID(data.userId),
            forAdminsOnly: data.isForAdmin,
            name: data.name,
        });
        return survey.save().catch((error) => {
            throw { code: 500, message: `Problem Creating survey`, technicalMessage: error.message };
        }).then((dbSurvey) => {
            if (!dbSurvey) {
                throw { code: 500, message: 'Error in creating survey' };
            }
            return dbSurvey;
        });
    }

    public getReport(surveyId: string): Promise<any> {
        return this.Survey.findById(surveyId, 'name').exec()
            .then((survey) => {
                return this.Questions.find({ survey: new ObjectID(survey.id) },
                    'options _id title ')
                    .exec().then((questions) => {
                        const promises: Array<Promise<any>> = [];
                        questions.map((question) => {
                            const aggregatePromise = this.Votes.aggregate([
                                // tslint:disable:object-literal-key-quotes
                                {
                                    $match: { questionId: mongoose.Types.ObjectId(question._id) },
                                },
                                {
                                    $group: {
                                        _id: '$answer', count: { $sum: 1 },
                                        questionId: { $addToSet: '$questionId' },
                                    },
                                }, { $unwind: { path: '$questionId' } },
                                {
                                    $group: {
                                        _id: '$questionId',
                                        answers: {
                                            $push: { count: '$count', name: '$_id' },
                                        },
                                        total: { $sum: '$count' },
                                    },
                                }, { $project: { _id: 0 } },
                            ]).exec().then((votes: any) => {
                                if (votes.length > 0) {
                                    let answers = votes[0].answers;
                                    const selectedOptions = answers.map((answer: any) => answer.name);
                                    const remainingOptions = question.options.filter((option) => {
                                        // tslint:disable-next-line:triple-equals
                                        return selectedOptions.indexOf(option) == -1;
                                    }).map((option) => ({ name: option, count: 0 }));
                                    answers = answers.concat(remainingOptions);
                                    return Promise.resolve({
                                        answers,
                                        title: question.toObject().title,
                                        total: votes[0].total,
                                    });
                                } else {
                                    const answers = question.options
                                        .map((option) => ({ name: option, count: 0 }));
                                    return Promise.resolve({

                                        answers,
                                        title: question.toObject().title,
                                        total: 0,
                                    });
                                }
                            });
                            promises.push(aggregatePromise);
                        });
                        return Promise.all(promises).then((questionWithVotes) => {
                            return Promise.resolve({
                                name: survey.toObject().name,
                                questionArray: questionWithVotes,
                            });
                        });
                    });
            });

    }

}
