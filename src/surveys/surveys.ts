import express, { Request, Response } from 'express';
import { errorProcessor, tokenChecker } from '../utils';
import MongoSurveyOps from './surveyDb';

const router = express.Router();

const surveyOps = new MongoSurveyOps();

const getAllSurveys = (request: Request, response: Response) => {
    console.log(request.query.mini);
    surveyOps.getAll(request.query.mini).then((surveys: any) => response.send(surveys)).catch(errorProcessor(response));
};
router.get('/', tokenChecker, getAllSurveys);

const getSurveyOrReport = (request: Request, response: Response) => {
    const id = request.params.id;
    console.log(request.query.mini);
    // tslint:disable-next-line:triple-equals
    if (id == 'reports') {
        if (!request.params.decoded.isAdmin) {
            response.status(403).send({ code: 403, message: 'Only admins can see reports' });
        } else {
            const surveyId = request.query.surveyId;
            if (surveyId) {
                surveyOps.getReport(surveyId).then((survey) => { response.send(survey); })
                    .catch(errorProcessor(response));
            } else {
                response.status(401).send({ code: 401, message: 'This request is not supported' });
            }
        }
    } else {
        surveyOps.getById(id, request.query.mini).
            then((survey: any) => response.send(survey)).catch(errorProcessor(response));
    }
};
router.get('/:id', tokenChecker, getSurveyOrReport);

const createSurvey = (request: Request, response: Response) => {
    if (request.params.decoded.isAdmin) {
        surveyOps.createSurvey({
            isForAdmin: request.body.isForAdmin, name: request.body.name,
            userId: request.params.decoded.id,
        }).
            then((survey) => response.send(survey)).
            catch(errorProcessor(response));
    } else {
        response.status(403).send({ code: 403, message: 'Only admins can create surveys' });
    }
};
router.post('/create', tokenChecker, createSurvey);

export default router;
