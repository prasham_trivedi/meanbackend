import mongoose, { Document, Schema } from 'mongoose';
import { allowedTypes } from './config';

const schema = mongoose.Schema;

export const HeroSchema = new schema({
    channel: String,
    name: String,
});

export const UserSchema = new schema({
    email: { type: String, unique: true },
    password: { type: String, minlength: 8 },
    token: { type: String, default: '' },
    userName: { type: String, unique: true, maxlength: 12, default: this.email },
    userType: { type: schema.Types.ObjectId, ref: 'UserType' },
    userTypeId: String,
}, { timestamps: true, toObject: { versionKey: false }, toJSON: { versionKey: false } });

export interface User extends Document {
    email: string;
    password: string;
    token: string;
    userName: string;
    UserType: string;
    userTypeId: string;
}

export const UserTypeSchema = new schema({
    isAdmin: Boolean,
    name: String,
    type: String,
}, { timestamps: true, toObject: { versionKey: false }, toJSON: { versionKey: false } });

const UserType = mongoose.model('UserType', UserTypeSchema, 'userTypes');
const User = mongoose.model('User', UserSchema, 'users');

export const SurveySchema = new schema({
    createdBy: { type: schema.Types.ObjectId, ref: 'User' },
    forAdminsOnly: Boolean,
    name: { type: String, minlength: 10, maxlength: 1000 },
}, {
        timestamps: true,
        // tslint:disable-next-line:object-literal-sort-keys
        collection: 'surveys', toObject: { versionKey: false }, toJSON: { versionKey: false },
    });

const Survey = mongoose.model('Survey', SurveySchema);
export interface Survey extends Document {
    createdBy: string;
    forAdminsOnly: boolean;
    name: string;
}
export const QuestionAnswersSchema = new schema({
    answer: String,
    userId: { type: schema.Types.ObjectId, ref: 'User' },
}, {
        timestamps: true,
        // tslint:disable-next-line:object-literal-sort-keys
        collection: 'answers', toObject: { versionKey: false }, toJSON: { versionKey: false },
    });

const Answer = mongoose.model('Answer', QuestionAnswersSchema);
export interface QuestionAnswers extends Document {
    answer: string;
    userId: string;
}

export const QuestoinsSchema = new schema({
    addedBy: { type: schema.Types.ObjectId, ref: 'User' },
    answer: [QuestionAnswersSchema],
    isRequired: { type: Boolean, default: true },
    options: { type: [String], default: [] },
    survey: { type: schema.Types.ObjectId, ref: 'Survey' },
    title: { type: String, minlength: 10, maxlength: 1000 },
    type: { type: String, enum: allowedTypes },
}, {
        timestamps: true,
        // tslint:disable-next-line:object-literal-sort-keys
        collection: 'questions', toObject: { versionKey: false }, toJSON: { versionKey: false },
    });
const Question = mongoose.model('Question', QuestoinsSchema);

export interface Question extends Document {
    addedBy: string;
    answer: [QuestionAnswers];
    isRequired: boolean;
    options: [string];
    survey: string;
    title: string;
    type: string;
}

export const Vote = new schema({
    answer: String,
    answerId: { type: schema.Types.ObjectId, ref: 'Answer' },
    questionId: { type: schema.Types.ObjectId, ref: 'Question' },
    userId: { type: schema.Types.ObjectId, ref: 'User' },
}, {
        timestamps: true,
        // tslint:disable-next-line:object-literal-sort-keys
        collection: 'votes', toObject: { versionKey: false }, toJSON: { versionKey: false },
    });

export interface Votes extends Document {
    answer: string;
    answerId: string;
    questoinId: string;
    userId: string;
}
