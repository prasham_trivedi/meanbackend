import { ObjectID, ObjectId } from 'bson';
import mongoose, { Document } from 'mongoose';
import {
    Question, QuestionAnswers, QuestionAnswersSchema,
    QuestoinsSchema, Survey, SurveySchema, Vote, Votes,
} from '../models';
export default class QuestionOps {

    private Questions = mongoose.model<Question>('question', QuestoinsSchema);
    private Answers = mongoose.model<QuestionAnswers>('answers', QuestionAnswersSchema);
    private Survey = mongoose.model<Survey>('surveys', SurveySchema);
    private VoteSchema = mongoose.model<Votes>('vote', Vote);

    constructor() {
        mongoose.set('debug', true);

    }
    public insert(question: any): Promise<any> {
        const questionForDb = new this.Questions({
            addedBy: new ObjectID(question.userId),
            isRequired: question.isRequired,
            options: question.options,
            survey: new ObjectID(question.surveyId),
            title: question.title,
            type: question.type,
        });
        return questionForDb.save().catch((error) => {
            throw { code: 500, message: `Error in Saving Question`, technicalMessage: error.message };
        }).then((savedQuestion) => {
            if (!savedQuestion) {
                throw { code: 500, message: `Question not saved` };
            }
            return savedQuestion;
        });
    }

    public getBySurveyId(surveyId: string): Promise<any> {

        return this.Survey.findById(surveyId, 'name').exec()
            .then((survey) => {
                return this.Questions.find({ survey: new ObjectID(survey.id) },
                    'isRequired options _id title type')
                    .exec()
                    .then((questions) => {
                        return Promise.resolve({
                            _id: survey.id,
                            name: survey.toObject().name,
                            questions,
                        });
                    })
                    .catch((error) => {
                        throw {
                            code: 500, message: `Error while finding questions for this survey`,
                            technicalMessage: error.message,
                        };
                    });
            });
    }

    public answerQuestion(answerId: string, userId: string, questionId: string, answer: string): Promise<any> {
        return this.Questions.findById(questionId).exec()
            .then((question) => {
                if (answerId == null || answerId === undefined) {
                    const answerModel = new this.Answers({
                        _id: new ObjectID(answerId),
                        answer,
                        userId,
                    });
                    if (question) {
                        question.answer.push(answerModel);
                        return question.save().then((savedQuestion) => {
                            return this.saveVote(savedQuestion, answerModel).then(() => {
                                return savedQuestion;
                            });
                        }).catch((error) => {
                            throw { code: 500, message: 'Error while saving answer', technicalMessage: error.message };
                        });
                    } else {
                        return Promise.reject({ code: 400, message: `No question found with ID ${questionId}` });
                    }
                } else {
                    return this.Questions.findOneAndUpdate({
                        'answer._id': new ObjectId(answerId),
                    }, { answer: { _id: answerId, answer } })
                        .then((savedQuestion) => {
                            return this.saveVote(savedQuestion, { _id: answerId, answer }).then(() => {
                                return savedQuestion;
                            });
                        }).catch((error) => {
                            throw {
                                code: 500, message: `Error while finding answer for ${answerId}`,
                                technicalMessage: error.message,
                            };
                        });
                }
            });
    }

    private saveVote(question: mongoose.Document, answerModel: any): Promise<any> {
        const questionObj = question.toObject();
        const answerId = answerModel._id;
        const questionId = questionObj._id;
        const userId = answerModel.userId;
        const answer = answerModel.answer;
        return this.VoteSchema
            .findOneAndUpdate({ answerId }, { answerId, questionId, userId, answer }, { upsert: true }).exec();
    }

}
