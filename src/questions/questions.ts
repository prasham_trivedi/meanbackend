import express, { Request, Response } from 'express';
import { check } from 'express-validator/check';
import { errorProcessor, tokenChecker, validator } from '../utils';
import QuestionOps from './questionsDb';

const router = express.Router();

const questionOps = new QuestionOps();

const questionValidationParams = [
    check('surveyId').exists().withMessage(`Can't create question without survey`),
    check('title').isLength({ min: 10, max: 1000 })
        .withMessage(`Question title should be between 10 to 1000 characters`),
    validator,
];
const questionValidationArrayParams = [
    check('surveyId').exists().withMessage(`Can't create question without survey`),
    check('questions.*.title').isLength({ min: 10, max: 1000 })
        .withMessage(`Question title should be between 10 to 1000 characters`),
    validator,
];

const createQuestion = (request: Request, response: Response) => {
    const body = request.body;
    const dbRequest = {
        isRequired: body.isRequired,
        options: body.options,
        surveyId: body.surveyId,
        title: body.title,
        type: body.type,
        userId: request.params.decoded.id,
    };
    questionOps.insert(dbRequest).then((insertedQuestion) => { response.send(insertedQuestion); })
        .catch(errorProcessor(response));
};

const getBySurveyId = (request: Request, response: Response) => {
    const surveyId = request.query.surveyId;
    if (surveyId) {
        questionOps.getBySurveyId(surveyId).then((survey) => { response.send(survey); })
            .catch(errorProcessor(response));
    } else {
        response.status(401).send({ code: 401, message: 'This request is not supported' });
    }
};

const answerQuestion = (request: Request, response: Response) => {
    const questionId = request.body.questionId;
    const userId = request.params.decoded.id;
    const answers = request.body.answers;
    const answerId = request.body.answerId;
    questionOps.answerQuestion(answerId, userId, questionId, answers).
        then((savedAnswer) => { response.send(savedAnswer); })
        .catch(errorProcessor(response));
};

const saveAllQuestions = (request: Request, response: Response) => {
    const questionArray = request.body.questions;
    const surveyId = request.body.surveyId;
    const promises: Array<Promise<any>> = [];
    questionArray.forEach((question: any) => {
        const dbRequest = {
            isRequired: question.isRequired,
            options: question.options,
            surveyId,
            title: question.title,
            type: 'radio',
            userId: request.params.decoded.id,
        };
        promises.push(questionOps.insert(dbRequest));
    });

    Promise.all(promises).then((questions) => response.send(questions)).catch(errorProcessor(response));
};

const answerAllQuestions = (request: Request, response: Response) => {
    const answersArray = request.body;
    const userId = request.params.decoded.id;
    const promises: Array<Promise<any>> = [];
    answersArray.forEach((element: any) => {
        const questionId = element.questionId;
        const answers = element.answers;
        const answerId = element.answerId;
        promises.push(questionOps.answerQuestion(answerId, userId, questionId, answers));
    });
    Promise.all(promises).then((answers) => response.send(answers)).catch(errorProcessor(response));
};

router.post('/create', tokenChecker, questionValidationParams, createQuestion);
router.get('/', tokenChecker, getBySurveyId);
router.post('/answer', tokenChecker, answerQuestion);
router.post('/answerAll', tokenChecker, answerAllQuestions);
router.post('/createAll', tokenChecker, questionValidationArrayParams, saveAllQuestions);

export default router;
