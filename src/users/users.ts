import express, { Request, Response } from 'express';
import { check } from 'express-validator/check';
import { errorProcessor, getToken, tokenChecker, validator } from '../utils';
import MongoUserOps from './userDb';

const router = express.Router();

const userOps = new MongoUserOps();

const signUpFunction = (request: Request, response: Response) => {
    const body = request.body;
    const signupData = { isAdmin: false, email: body.email, password: body.password, userName: body.userName };
    userOps.createUser(signupData).then((data: any) => { response.send(data); }).catch(errorProcessor(response));
};

const loginFunction = (request: Request, response: Response) => {
    const body = request.body;
    userOps.ifUserExists(body).then((user) => {
        response.json({
            // tslint:disable-next-line:object-literal-sort-keys
            id: user._id, email: user.email,
            userType: user.userType, isAdmin: user.isAdmin, token: user.token, userName: user.userName,
        });
    }).
        catch(errorProcessor(response));
};

const createValidationParams = [check('email').isEmail().withMessage('Should be proper email'),
check('password').isLength({ min: 8 }).withMessage('Password must not be empty, and with minimum 8 characters'),
check('userName').optional().isString().withMessage('Username must be a string'),
check('userName').isLength({ max: 12 }).withMessage('Usernames must not exceed 12 characters')
    , validator];

const createUserFunction = (request: Request, response: Response) => {
    console.log(request.params.decoded.isAdmin);
    if (!request.params.decoded.isAdmin) {
        response.status(403).send({ code: 403, message: 'Only admins can create users' });
    }
    const body = request.body;
    userOps.createUser(body).then((data: any) => { response.send(data); }).catch(errorProcessor(response));
};
const helloFunction = (request: Request, response: Response) => {
    console.log(JSON.stringify(request.params.decoded));
    response.send('Hello');
};

const loginValidationParams = [check('email').isEmail().withMessage('Should be proper email'),
check('password').isLength({ min: 8 }).withMessage('Password must not be empty, and with minimum 8 characters')
    , validator];

router.post('/login', loginValidationParams, loginFunction);

router.post('/create', tokenChecker, createValidationParams, createUserFunction);

const signupValidationParams = [check('email').isEmail().withMessage('Should be proper email'),
check('password').isLength({ min: 8 }).withMessage('Password must not be empty, and with minimum 8 characters'),
check('userName').optional().isString().withMessage('Username must be a string'),
check('userName').isLength({ max: 12 }).withMessage('Usernames must not exceed 12 characters'),
validator];

router.post('/signup', signupValidationParams, signUpFunction);

router.get('/hello', tokenChecker, helloFunction);

export default router;
