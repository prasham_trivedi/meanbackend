import mongoose from 'mongoose';
import { User, UserSchema } from '../models';
import { getToken } from '../utils';
export default class MongoUserOps {
    private user = mongoose.model<User>('users', UserSchema);
    constructor() {
        mongoose.set('debug', process.env.isDebug);
    }

    public cleanup(): Promise<any> {
        return this.user.remove({}).exec();
    }

    public createUser(data: any): Promise<any> {
        let reqUserTypeId = '5c18c0e548e1c5038d1f5dbe';
        if (!data.isAdmin) {
            reqUserTypeId = '5c18c10a48e1c5038d1f5dbf';
        }
        let name = data.userName;
        if (!name) {
            name = data.email.substring(0, 12);
        }

        const user = new this.user({
            email: data.email, password: data.password, userName: name, userType: Object(reqUserTypeId),
            userTypeId: reqUserTypeId,
        });
        return user.save().catch((error) => {
            throw { code: 500, message: 'Can not save user.', technicalMessage: error.message };
        }).then((dbUser) => {
            if (dbUser) {
                return {
                    _id: dbUser.id, email: dbUser.email, token: dbUser.token
                    , userName: dbUser.userName, userType: dbUser.userTypeId,
                };
            } else {
                throw { code: 500, message: 'Something went wrong while creating user' };
            }
        });
    }

    public ifUserExists(data: any): Promise<any> {
        return this.user.findOne({ email: data.email, password: data.password }, '')
            .populate('userType').exec()
            .catch((error) => {
                throw { code: 500, message: 'Something went wrong while logging in', technicalMessage: error.message };
            }).then((returnedUser) => {
                if (!returnedUser) {
                    throw { code: 400, message: 'User with given email/password is not found' };
                }
                const user = returnedUser.toObject();
                if (user.token) {
                    return {
                        _id: user._id, email: user.email,
                        isAdmin: user.userType.isAdmin,
                        token: user.token,
                        userName: user.userName,
                        userType: user.userType.type,
                    };
                } else {
                    const userToken = getToken(user);
                    returnedUser.token = userToken;
                    return returnedUser.save().then((updatedUser) => {
                        return {
                            _id: user._id, email: user.email,
                            isAdmin: user.userType.isAdmin,
                            token: userToken,
                            userName: user.userName,
                            userType: user.userType.type,
                        };
                    }).catch((error) => {
                        throw { code: 500, message: `Error while logging in`, technicalMessage: error.message };
                    });
                }
            });
    }

}
